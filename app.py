from ast import Try
from flask import Flask, request
from waitress import serve

import socket
import struct
import time

app = Flask(__name__)


def get_ip_address():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("192.168.0.2", 80))
    return s.getsockname()[0]


def RequestTimefromNtp(addr='0.de.pool.ntp.org'):
    tme = "timeless"
    try:
        REF_TIME_1970 = 2208988800  # Reference time
        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        data = b'\x1b' + 47 * b'\0'
        client.sendto(data, (addr, 123))
        data, address = client.recvfrom(1024)
        if data:
            t = struct.unpack('!12I', data)[10]
            t -= REF_TIME_1970
            tme = time.ctime(t)
    except Exception as e:
        tme = str(e)

    return tme, addr


@app.route('/')
def hello():
    t, a = RequestTimefromNtp()
    http = f"<center><h1>VISMIG OCP4 CRC Test-Deployment</h1>"
    http += f"<hr>"
    http += f"<p>Die aktuelle Zeit auf {a} ist <b>{t} UTC</b></p>"
    http += f"<p>Die interne IP-Adresse des Pods ist <b>{get_ip_address()}</b></p>"
    http += f"<hr>"
    # ------------------------------------------------------------------------
    # Robert: Hier kannst Du eine Änderung einbauen und committen - die sollte 
    #         dann reativ schnell in der Anwendung 
    #
    #         https://ocp4apps.herrrrausb.de/hello
    #
    #         sichtbar werden...
    #
    # http += f"<p>NEUER INHALT</p>"
    #
    # ------------------------------------------------------------------------
    http += f"</center>"
    return http


if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=8080)
